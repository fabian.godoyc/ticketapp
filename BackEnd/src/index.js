//Voy a requerir express
const express = require('express');
const cors = require("cors");

//Para utilizar morgar, es como importarlo 
const morgan= require('morgan')

//importar el modulo taskroutes lo requeriremos de la direccion  ./routes/task.routes
const routes = require('./routes/routes');

//Ejecuto express
const app = express();

app.use(cors({origins:["http://localhost:3000/"]}))
//Ejecuta morgan, muestra por consola las peticiones que van llegando

app.use(morgan('dev'))
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//Aplicacion quiero que uses taskRoutes
app.use(routes);


//Escuchar express en el puerto 3000
app.listen(4000)
console.log('Server on port 4000')

