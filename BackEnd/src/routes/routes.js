//funcion router
const {Router} = require('express');
const pool = require("../db");
//Este objeto router permitira crear urls
const router = Router();

//obtener datos
router.get('/account', async (req, res) => {
    const result = await pool.query("SELECT * FROM accounts")
    res.json(result.rows)
});

//posicion del usuario
router.get('/list/:username', async (req, res) => {
    const { username } = req.params
    const result = await pool.query("SELECT * FROM list WHERE username = $1", [username])
    res.json(result.rows[0].position)
});

//obtener lista de espera
router.get('/list', async (req, res) => {
    const result = await pool.query("SELECT username, position FROM list ORDER BY position asc")
    res.json(result.rows)
});

//registrar
router.post('/register', async (req, res) => {
    const { username, password, email } = req.body;
    const result = await pool.query("INSERT INTO accounts (username, email, password) VALUES ($1, $2, $3)", [
        username,
        email,
        password
    ])
    res.json("executed")
});

//correr la lista
router.put('/list', async (req, res) => {
    const result = await pool.query("SELECT * FROM list")
    for (var i = 0; i < result.rows.length; i++) {
        if(i == 0){
            var myPythonScript = "script.py";
            var pythonExecutable = "python.exe";
            const del = await pool.query("DELETE FROM list WHERE username=$1", [result.rows[i].username])
            const em = await pool.query("SELECT email FROM accounts WHERE username=$1", [result.rows[i].username])
            //procedes a avisarle por mail
            // The path to your python script
            var myPythonScript = "script.py";
            // Proporciona la ruta del ejecutable de Python, si Python está disponible como variable de entorno, entonces puedes usar solo "Python"
            var pythonExecutable = "python.exe";

            // Función de convertir un Uint8Array a string
            var uint8arrayToString = function(data){
                return String.fromCharCode.apply(null, data);
            };

            const spawn = require('child_process').spawn;
            const scriptExecution = spawn(pythonExecutable, [myPythonScript, em.rows[0].email] );

            // Manejo normal del output
            scriptExecution.stdout.on('data', (data) => {
                console.log(uint8arrayToString(data));
            });

            // Manejo del output erimage.pngror
            scriptExecution.stderr.on('data', (data) => {
                // As said before, convert the Uint8Array to a readable string.
                console.log(uint8arrayToString(data));
            });

            scriptExecution.on('exit', (code) => {
                console.log("Process quit with code : " + code);
            });
            console.log(em.rows[0].email)
            process.stdout.on('data', function(data) { 
                res.send(data.toString());
            }) 



        }
        else{
            const resu = await pool.query("UPDATE list SET position = $1 WHERE username=$2", [
                result.rows[i].position - 1,
                result.rows[i].username
            ])
        }          
        
     }
    res.json("executed")
});

//añadir a fila
//la fila no puede estar vacio****
router.post('/list', async (req, res) => {
    const {username} = req.body;
    const pos = await pool.query("SELECT position FROM list ORDER BY position desc LIMIT 1");
    var position = pos.rows[0].position + 1
    const result = await pool.query("INSERT INTO list (username, position) VALUES ($1, $2)", [username, position]);
    res.json("executed")
});


//salir de la cola
//el usuario sale de la cola, los usuarios bajo el tienen que disminuir en 1 su posicion update

router.put('/out/:username', async (req, res) => {
    const { username } = req.params
    const result_user = await pool.query("SELECT * FROM list WHERE username = $1", [username])
    pos=result_user.rows[0].position
    const result = await pool.query("SELECT * FROM list WHERE position > $1", [pos])
    const del = await pool.query("DELETE FROM list WHERE username=$1", [username])
    for(var i = 0; i < result.rows.length; i++){
        const resu = await pool.query("UPDATE list SET position = $1 WHERE username=$2", [
            result.rows[i].position - 1,
            result.rows[i].username
            ])
        }    
    res.json("executed")
});

module.exports = router;