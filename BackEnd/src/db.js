//Clase llamada pool que permite crear una conexion

const {Pool} = require('pg')

//al ejecutarlo recibe un objeto de configuracion, osea el usuarios , contrasenia, etc de la base de datos
const pool = new Pool({
    user: 'postgres',
    password: 'peluda123',
    host: 'database-1.cqgvx9pmr2tt.us-east-2.rds.amazonaws.com',
    port: 5432,
    database: "postgres"
});

module.exports = pool;