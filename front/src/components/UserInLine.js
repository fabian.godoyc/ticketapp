import { Button, Container, Row} from "react-bootstrap"

function UserInLine (props){
    return(
        <section className="Num" style={{top:'15%',left:'50%'}}>
            <Container>
                <Row><label>Wait Time</label></Row>
                <Row><h2>{props.time}</h2></Row>
                <Row><label>Ticket</label></Row>
                <Row><h2>{props.pos}</h2></Row>
                <Row><h1><Button style={{top:'15%',left:'10%',right:'10%'}} size="m" onClick={() => props.back()}>Back</Button></h1> </Row>
            </Container>
        </section>
    )
}

export default UserInLine