import { Row,Container,Button } from "react-bootstrap"

function CredentialLogin (props){
    return(
    <section className="credentialLogin" style={{top:'15%',left:'50%'}}>
        <Container>
            <Row><label>Name</label></Row>
            <Row style={{top:'15%',left:'10%',right:'10%'}}><input type="text" class="domTextElement" name="uname" required/></Row>
            <Row><label>Password</label></Row>
            <Row style={{top:'15%',left:'10%',right:'10%'}}><input type="text" class="domTextElement" name="upass" required/></Row>
            <Row><h1><Button size="m" onClick={() => 
                props.login(document.getElementsByClassName("domTextElement")[0].value,
                            document.getElementsByClassName("domTextElement")[1].value)}>Login</Button></h1> </Row>
            <Row><h1><Button size="m" onClick={() => props.signup()}>Signup</Button></h1> </Row>
            <Row><h1><Button size="m" onClick={() => props.admin()}>Line Admin</Button></h1> </Row>
        </Container>
    </section>
    )
    
}
export default CredentialLogin