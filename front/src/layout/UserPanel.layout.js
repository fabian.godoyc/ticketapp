import { useState, useEffect } from "react"
import CredentialLogin from "../components/credentialLogin"
import UserInLine from "../components/UserInLine"
import AdminView from "../components/AdminView"
import Register from "../components/Register"
import axios from "axios"

//conseguir de db
//const dbUsers = [{name:"a",pass:"a"},{name:"b",pass:"b"},{name:"c",pass:"c"},{name:"d",pass:"d"}]
//let Line = [{name:"q",ticket:1},{name:"a",ticket:2},{name:"c",ticket:3},{name:"j",ticket:4}]

function UserPanel () {
    
    const [user,setUser] = useState("")
    const [existUser,setExistUser] = useState(false)
    const [userlogged,setUserLogged] =useState(0)
    const [ticket, setTicket] = useState(0)
    const [first,setFirst] = useState(0)
    const [time,setTime] = useState(0)
    const [list,setList] = useState([{username:"q",position:1},{username:"a",position:2},{username:"c",position:3},{username:"j",position:4}])
    const [dbUsers,setDbUsers] = useState([])
    
    const checkUser = (name, pass) => {
        setExistUser(false)
        const fetchData = async () => {
            const result = await axios.get('http://localhost:4000/account')
            if (result.data) {
                setDbUsers(result.data)
                return (result.data)
            }
            else{
                return[]
            }
        }
        fetchData()
        console.log(dbUsers)
        dbUsers.forEach(dbUser =>{
            if (dbUser.username == name & dbUser.password == pass){
                setUser(name)
                setExistUser(true)
            }
        })
        if(existUser == false){
            console.log("no existe este")
        }
        else{
            getTicket(user)
            setUserLogged(1)
        }      
    }
    
    const getTicket = (name) => {
        setTicket(0)
        list.forEach(user => {
            if(name == user.nusername){
                setTicket(user.position)
            }
        });
        if(ticket == 0){
            setTicket(list[list.length-1].position+1)
            const fetchData2 = async () => {
                const result = await axios.post('http://localhost:4000/list',{username:user})
                if (result.data) {
                    return(result.data)
                }
            }
            fetchData2()

        } 
         
        console.log(list,ticket)
    }

    const getdata = () => {
        console.log(ticket,first,list)
        //  console.log(Line)
    }

    const goAdmin = () => {
        setUserLogged(2)
    }

    const nextNumber = () => {
        if(list.length>0){
            const fetchData = async () => {
                const result = await axios.put('http://localhost:4000/list')
            }
            fetchData()
            setTicket(ticket-1)
        }
        
    }
    
    const signUp = (name,pass,email) => {
        const fetchData = async () => {
            const result = await axios.post('http://localhost:4000/register',{username:name, password:pass, email:email})
        }
        fetchData()
    }

    const goIndex = () =>{
        setUserLogged(0)
    }
    const goRegister = () =>{
        setUserLogged(3)
    }

    useEffect(() => {
        const fetchData = async () => {
                const result = await axios.get('http://localhost:4000/list')
                if (result.data) {
                    setList(result.data)
                }
        }
        fetchData()
        list.forEach(user1 => {
            if(user == user1.username){
                setTicket(user1.position)
            }
        });
        if(ticket>=0){
            setTime(String((ticket-1)*3)+" min")
        }
        //console.log(result.data)
    })

    if(userlogged == 0){
        return(
            <div className="Login">
                <CredentialLogin login={checkUser} admin={goAdmin} signup={goRegister}/>
            </div> 
        )
    }
    else if(userlogged == 1){
        return(
            <div className="Line">
                <UserInLine pos={ticket} time={time} getdata ={getdata} back={goIndex}/>
            </div>
        )
    }
    else if(userlogged == 2){
        return(
            <div className="Admin">
                <AdminView  first={list.lengthimage.png}  nextNumber ={nextNumber} back={goIndex} getdata={getdata}/>
            </div>
        )
    }
    else{
        return(
            <div className="SignUp">
                <Register signup ={signUp} back={goIndex} />
            </div>
        )
    }

}

export default UserPanel