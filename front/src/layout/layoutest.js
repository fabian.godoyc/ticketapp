import { useState } from "react"
import axios from "axios"
import CredentialLogin from "../components/credentialLogin"
function Testinglayout () {
    
    const [user,setUser] = useState("")
    const [existUser,setExistUser] = useState(false)
    const [userlogged,setUserLogged] =useState(0)
    const [ticket, setTicket] = useState(0)
    const [first,setFirst] = useState(0)
    const [time,setTime] = useState(0)
    const [list,setList] = useState()

    const goAdmin = () => {
        console.log("a admin")
    }

    const checkUser = (name, pass) => {
        setExistUser(false)

        const fetchData = async () => {
            const result = await axios.get('http://localhost:4000/account')
            console.log(result)
            if (result.data) {
                return (result.data)
            }
            else{
                return[]
            }
        }
        console.log(fetchData())        
    }

    return(
        <div className="Login">
            <CredentialLogin login={checkUser} admin={goAdmin}/>
        </div> 
    )
}

export default Testinglayout