
import Header from './layout/header'
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css'
import UserPanel from './layout/UserPanel.layout';
import Testinglayout from './layout/layoutest';

function App() {
  return (
    <div className="App">
      <Header course="Ticket" detail="jamon queso"/>
      <UserPanel/>
    </div>
  )
}

export default App;
