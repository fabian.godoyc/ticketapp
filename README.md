# TicketAPP: Gestor de filas multiproposito

#### Proyecto DISEÑO DE APLICACIONES WEB Y MÓVILES

Aplicacion que permite manejar colas de comercios, el encolamiento se realiza mediante lectura de codigo QR, el cual redigira al usuario a la fila correspodiente, una vez dentro este tendra la opcion de registarse o encolarse si dispone de un usuario.

Integrantes:

Phillipa Paredes

Fabian Godoy

## Back:

Back desarrollado en NodeJs, la aplicacion se encuentra configurada por defecto en localhost:4000/, Permite el acceso y control de usuarios y las filas.

#### Ejecucion de BackEnd:

cd ./BACKEND

npm install

npm start 

## Front:

Front desarrollado en React Js, la aplicacion por defecto se despliega en localhost:3000/ presentando una pagina de Login.

IMPORTE: al crear una cuenta el correo debe existir, sino afectara el funcionamiento de notificacion por email y el del backend.

#### Ejecucion de FrontEnd:

cd ./front

npm install

npm start

